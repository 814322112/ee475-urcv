/*
  Debounce

  Each time the input pin goes from LOW to HIGH (e.g. because of a push-button
  press), the output pin is toggled from LOW to HIGH or HIGH to LOW. There's a
  minimum delay between toggles to debounce the circuit (i.e. to ignore noise).

  The circuit:
  - LED attached from pin 13 to ground
  - pushbutton attached from pin 2 to +5V
  - 10 kilohm resistor attached from pin 2 to ground

  - Note: On most Arduino boards, there is already an LED on the board connected
    to pin 13, so you don't need any extra components for this example.

  created 21 Nov 2006
  by David A. Mellis
  modified 30 Aug 2011
  by Limor Fried
  modified 28 Dec 2012
  by Mike Walters
  modified 30 Aug 2016
  by Arturo Guadalupi

  This example code is in the public domain.

  http:int www.arduino.cc/en/Tutorial/Debounce
*/

//  constants won't change. They're used here to set pin numbers:
int buttonState = 0; 
double LEFT_UP = 1.05;
double LEFT_DOWN = 1.05;
double RIGHT_UP = 1.05;
double RIGHT_DOWN = 1.05;
int BUTTON1 = 5;
int LEFT_UP_FRONT = 0;
int LEFT_UP_BACK = 1;
int LEFT_DOWN_FRONT = 20;
int LEFT_DOWN_BACK = 21;
int RIGHT_UP_FRONT = 15;
int RIGHT_UP_BACK = 16;
int RIGHT_DOWN_FRONT = 22;
int RIGHT_DOWN_BACK = 23;
void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(LED_BUILTIN+1, OUTPUT);
  pinMode(LED_BUILTIN-1, OUTPUT);
  fpioa_set_function (LEFT_UP_FRONT , FUNC_TIMER0_TOGGLE1);
  fpioa_set_function (LEFT_UP_BACK , FUNC_TIMER0_TOGGLE2);
  fpioa_set_function (LEFT_DOWN_FRONT , FUNC_TIMER0_TOGGLE3);
  fpioa_set_function (LEFT_DOWN_BACK , FUNC_TIMER0_TOGGLE4);
  fpioa_set_function (RIGHT_UP_FRONT , FUNC_TIMER1_TOGGLE1);
  fpioa_set_function (RIGHT_UP_BACK , FUNC_TIMER1_TOGGLE2);
  fpioa_set_function (RIGHT_DOWN_FRONT , FUNC_TIMER1_TOGGLE3);
  fpioa_set_function (RIGHT_DOWN_BACK , FUNC_TIMER1_TOGGLE4);
  pwm_init ( PWM_DEVICE_0 );
  pinMode(BUTTON1, INPUT);
  pwm_set_enable ( PWM_DEVICE_0 , PWM_CHANNEL_0 , 0);
  pwm_set_enable ( PWM_DEVICE_0 , PWM_CHANNEL_1 , 0);
  pwm_set_enable ( PWM_DEVICE_0 , PWM_CHANNEL_2 , 0); 
  pwm_set_enable ( PWM_DEVICE_0 , PWM_CHANNEL_3 , 0);
  pwm_set_enable ( PWM_DEVICE_1 , PWM_CHANNEL_0 , 0);
  pwm_set_enable ( PWM_DEVICE_1 , PWM_CHANNEL_1 , 0);
  pwm_set_enable ( PWM_DEVICE_1 , PWM_CHANNEL_2 , 0); 
  pwm_set_enable ( PWM_DEVICE_1 , PWM_CHANNEL_3 , 0);
    pwm_set_enable ( PWM_DEVICE_0 , PWM_CHANNEL_0 , 1);
    pwm_set_enable ( PWM_DEVICE_0 , PWM_CHANNEL_1 , 1);
    pwm_set_enable ( PWM_DEVICE_0 , PWM_CHANNEL_2 , 1); 
    pwm_set_enable ( PWM_DEVICE_0 , PWM_CHANNEL_3 , 1);
    pwm_set_enable ( PWM_DEVICE_1 , PWM_CHANNEL_0 , 1);
    pwm_set_enable ( PWM_DEVICE_1 , PWM_CHANNEL_1 , 1);
    pwm_set_enable ( PWM_DEVICE_1 , PWM_CHANNEL_2 , 1); 
    pwm_set_enable ( PWM_DEVICE_1 , PWM_CHANNEL_3 , 1);
  left_up_motor(80*LEFT_UP);
  left_down_motor(80*LEFT_DOWN);
  right_up_motor(80*RIGHT_UP);
  right_down_motor(80*RIGHT_DOWN);

  //

 Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(LED_BUILTIN+1, HIGH);
  digitalWrite(LED_BUILTIN-1, HIGH);
  gpio_init ();
  fpioa_set_function (27 , FUNC_GPIO0 );
  gpio_set_drive_mode (0 , GPIO_DM_INPUT );
  fpioa_set_function (28 , FUNC_GPIO1 );
  gpio_set_drive_mode (1 , GPIO_DM_INPUT );
  fpioa_set_function (29 , FUNC_GPIO2 );
  gpio_set_drive_mode (2 , GPIO_DM_INPUT );
  fpioa_set_function (30 , FUNC_GPIO3 );
  gpio_set_drive_mode (3 , GPIO_DM_INPUT );
  movestop();
}

 void movefront(){
  left_up_motor(80*LEFT_UP);
  left_down_motor(80*LEFT_DOWN);
  right_up_motor(80*RIGHT_UP);
  right_down_motor(80*RIGHT_DOWN);
 }
 void moveback(){
  left_up_motor(-80*LEFT_UP);
  left_down_motor(-80*LEFT_DOWN);
  right_up_motor(-80*RIGHT_UP);
  right_down_motor(-80*RIGHT_DOWN);
 }

 void turnleft(){
  left_up_motor(0*LEFT_UP);
  left_down_motor(0*LEFT_DOWN);
  right_up_motor(80*RIGHT_UP);
  right_down_motor(80*RIGHT_DOWN);
 }

 void turnright(){
  left_up_motor(80*LEFT_UP);
  left_down_motor(80*LEFT_DOWN);
  right_up_motor(0*RIGHT_UP);
  right_down_motor(0*RIGHT_DOWN);
 }

 void movestop(){
  left_up_motor(0*LEFT_UP);
  left_down_motor(0*LEFT_DOWN);
  right_up_motor(0*RIGHT_UP);
  right_down_motor(0*RIGHT_DOWN);
 }
 
 void left_up_motor(double v){
  if (v > 0){
    pwm_set_frequency ( PWM_DEVICE_0 , PWM_CHANNEL_0 , 100 , v*1.0/100);
    pwm_set_frequency ( PWM_DEVICE_0 , PWM_CHANNEL_1 , 100 , 0);

  } else {
    pwm_set_frequency ( PWM_DEVICE_0 , PWM_CHANNEL_0 , 100 , 0);
    pwm_set_frequency ( PWM_DEVICE_0 , PWM_CHANNEL_1 , 100 , v*-1.0/100); 
  }
}

 void left_down_motor(double v){
  if (v > 0){
    pwm_set_frequency ( PWM_DEVICE_0 , PWM_CHANNEL_2 , 100 , v*1.0/100);
    pwm_set_frequency ( PWM_DEVICE_0 , PWM_CHANNEL_3 , 100 , 0);

  } else {
    pwm_set_frequency ( PWM_DEVICE_0 , PWM_CHANNEL_2 , 100 , 0);
    pwm_set_frequency ( PWM_DEVICE_0 , PWM_CHANNEL_3 , 100 , v*-1.0/100); 
  }
}

 void right_up_motor(double v){
    if (v > 0){
      pwm_set_frequency ( PWM_DEVICE_1 , PWM_CHANNEL_0 , 100 , v*1.0/100);
      pwm_set_frequency ( PWM_DEVICE_1 , PWM_CHANNEL_1 , 100 , 0); 
    } else {
      pwm_set_frequency ( PWM_DEVICE_1 , PWM_CHANNEL_0 , 100 , 0);
      pwm_set_frequency ( PWM_DEVICE_1 , PWM_CHANNEL_1 , 100 , v*-1.0/100);     
    } 
 }
 
void right_down_motor(double v){
    if (v > 0){
      pwm_set_frequency ( PWM_DEVICE_1 , PWM_CHANNEL_2 , 100 , v*1.0/100);
      pwm_set_frequency ( PWM_DEVICE_1 , PWM_CHANNEL_3 , 100 , 0); 
    } else {
      pwm_set_frequency ( PWM_DEVICE_1 , PWM_CHANNEL_2 , 100 , 0);
      pwm_set_frequency ( PWM_DEVICE_1 , PWM_CHANNEL_3 , 100 , v*-1.0/100);     
    } 
}

void loop() {
 gpio_pin_value_t value =  GPIO_PV_HIGH;
 gpio_pin_value_t up = gpio_get_pin (0);
 gpio_pin_value_t down = gpio_get_pin (1);
 gpio_pin_value_t left = gpio_get_pin (2);
 gpio_pin_value_t right = gpio_get_pin (3);

 if(value == up){
  movefront();digitalWrite(LED_BUILTIN, LOW);
  digitalWrite(LED_BUILTIN+1, HIGH);
  digitalWrite(LED_BUILTIN-1, HIGH);
 }else if(value == down){ 
  moveback();digitalWrite(LED_BUILTIN+1, LOW);
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(LED_BUILTIN-1, HIGH);
 }else if(value == right){
  turnright(); digitalWrite(LED_BUILTIN-1, LOW);
  digitalWrite(LED_BUILTIN+1, HIGH);
  digitalWrite(LED_BUILTIN, HIGH);
 }else if(value==left){
  turnleft(); 
 }else{
  movestop();
 }
 
}
